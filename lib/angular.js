import * as baseDirectives from './baseDirectives';

(function() {
  function SmallAngular() {
    const rootScope = window;
    const directives = {};
    const components = {};
    const watchers = {};
    const controllers = {};
    const services = {};

    function directive(name, fn) {
      const deformedKey = name.replace(/[A-Z]/g, match => `-${match.toLowerCase()}`);

      if (!directives[deformedKey]) {
        directives[deformedKey] = [];
      }
      directives[deformedKey].push(fn);
      return this;
    }

    function component(name, fn) {
      const deformedKey = name.replace(/[A-Z]/g, match => `-${match.toLowerCase()}`);

      if (!components[deformedKey]) {
        components[deformedKey] = [];
      }
      components[deformedKey].push(fn);
      return this;
    }

    function service(name, fn) {
      services[name] = fn;
    }
    function controller(name, fn) {
      if (!controllers[name]) {
        controllers[name] = [];
      }
      controllers[name].push(fn);
      return this;
    }

    rootScope.$apply = name => {
      for (const key in watchers) {
        watchers[key].forEach(fn => fn());
      }
    };

    rootScope.$watch = (name, cb) => {
      if (!watchers[name]) {
        watchers[name] = [];
      }
      watchers[name].push(cb);
      return this;
    };

    const initBaseDirectives = initDirect => {
      for (const key in initDirect) {
        this.directive(key, initDirect[key]);
      }
    };

    this.compile = node => {
      const directivesVsComponents = Object.assign(directives, components);
      const nodeAttrs = Object.values(node.attributes);
      nodeAttrs.forEach(el => {
        if (!directivesVsComponents[el.name]) {
          return;
        }

        const attributes = nodeAttrs.filter(atr => !atr.name.match('ng-') && !atr.name.match(el.name));
        directivesVsComponents[el.name].forEach(fn => {
          const callDirective = fn();

          if (typeof callDirective.controller === 'function') {
            callDirective.controller();
          }
          callDirective.link(rootScope, node, attributes);
        });
      });
    };

    this.bootstrap = (node = document.querySelector('[ng-app]')) => {
      const allNodeChildren = Array.from(node.getElementsByTagName('*'));
      const arrOfCustomAtr = Object.keys(Object.assign(directives, components));
      const customAtr = Array.from(node.attributes).find(el => arrOfCustomAtr.includes(el.name));

      if (!customAtr) {
        return;
      }
      this.compile(node);
      allNodeChildren.forEach(elem => {
        this.compile(elem);
      });
    };

    this.directive = directive;
    this.component = component;
    this.controller = controller;
    this.service = service;
    initBaseDirectives(baseDirectives);
    this.bootstrap();
  }

  window.angular = new SmallAngular();
}());


